function change(){
	const valor1 = document.getElementById('divisa1');
	const valor2 = document.getElementById('divisa2');
	let cont = "";
	if(parseInt(valor1.value) == 0){
        cont = `<option value="5">Peso mexicano</option>
        <option value="6">Dólar estadounidense</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option> `

    }if(parseInt(valor1.value) == 1){
        cont = `<option value="6">Dólar estadounidense</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(valor1.value) == 2){
        cont = `<option value="5">Peso mexicano</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(valor1.value) == 3){
        cont = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="8">Euro</option>`
    }if(parseInt(valor1.value) == 4){
        cont = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="7">Dolar canadiense</option>`
    }

	valor2.innerHTML = cont;
}

//codoficacion de la funcion calcular

function calcular(){
    let val = document.getElementById('cantidad').value
    let valor1 = document.getElementById('divisa1')
    let valor2 = document.getElementById('divisa2')
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')

    // Calcular Pesos
    if(parseInt(valor1.value) == 1 && parseInt(valor2.value) == 6){
        subtotal.value = val / 19.85
    }else if(parseInt(valor1.value) == 1 && parseInt(valor2.value) == 7){
        subtotal.value = val / 14.70
    }else if(parseInt(valor1.value) == 1 && parseInt(valor2.value) == 8){
        subtotal.value = val / 20.05
    }

    // Calcular Dolares
    else if(parseInt(valor1.value) == 2 && parseInt(valor2.value) == 5){
        subtotal.value = val * 19.85
    }else if(parseInt(valor1.value) == 2 && parseInt(valor2.value) == 7){
        subtotal.value = val * 1.35
    }else if(parseInt(valor1.value) == 2 && parseInt(valor2.value) == 8){
        subtotal.value = val * 0.99
    }

    // Calcular Dolares canadiences
    else if(parseInt(valor1.value) == 3 && parseInt(valor2.value) == 5){
        subtotal.value =  (val / 1.35) * 19.85
    }else if(parseInt(valor1.value) == 3 && parseInt(valor2.value) == 6){
        subtotal.value = (val / 1.35)
    }else if(parseInt(valor1.value) == 3 && parseInt(valor2.value) == 8){
        subtotal.value =  (val / 1.35) * 0.99
    }

    // Calcular Euro
    else if(parseInt(valor1.value) == 4 && parseInt(valor2.value) == 5){
        subtotal.value = (val / 0.99) * 19.85
    }else if(parseInt(valor1.value) == 4 && parseInt(valor2.value) == 6){
        subtotal.value = (val / 0.99)
    }else if(parseInt(valor1.value) == 4 && parseInt(valor2.value) == 7){
        subtotal.value = (val / 0.99) * 1.35
    }
    
    comision.value = (subtotal.value * .03).toFixed(2)
    pagar.value = (parseFloat(subtotal.value) + parseFloat(comision.value)).toFixed(2)

}

//valores globales
let subF = 0
let comF = 0
let pagarF = 0

//Funcion registrar
function registrar(){
    let val = document.getElementById('cantidad')
    let valor1 = document.getElementById('divisa1')
    let valor2 = document.getElementById('divisa2')
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')
    let registros = document.getElementById('registros')
    let totales = document.getElementById('totales')
    

    registros.innerText += `Valor de la moneda origen:  ${val.value} su total en la moneda elegida: ${subtotal.value}  comision cobrada:  ${comision.value} total indicado: ${pagar.value} \n`;

    subF += parseFloat(subtotal.value)
    comF += parseFloat(comision.value)
    pagarF += parseFloat(pagar.value)


    totales.innerText = ` Subtotal: ${parseFloat(subF)} Comision: ${parseFloat(comF)} Total: ${parseFloat(pagarF)}` 

}

//Boton borrar

function borrar(){
    let registros = document.getElementById('registros')
    let totales = document.getElementById('totales')
 
    registros.innerHTML = ''
    totales.innerHTML = ''
    document.getElementById('cantidad').value = "";
    document.getElementById('subtotal').value = "";
    document.getElementById('comision').value = "";
    document.getElementById('pagar').value = "";


}

//terminacion de la aplicacion
